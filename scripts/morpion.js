$("document").ready(function()
{
	var winningCombinations = [
	[0,1,2],
	[3,4,5],
	[6,7,8],

	//vertical
	[0,3,6],
	[1,4,7],
	[2,5,8],

	//diagonal
	[0,4,8],
	[2,4,6]
	];

	var choosenSquares = {'x':[], 'o': []}

	var currentPlayerToken = 'x';
	console.log(currentPlayerToken);

	$('#grid').on('click', ".cell:not('.cell-x, .cell-o')", function(event)
	{
		var $cell = $(this);
		$cell.addClass('cell-' + currentPlayerToken);

		var indexOfSquare = $('#grid .cell').index($cell);

		var currentPlayerSquares = choosenSquares[currentPlayerToken]
		currentPlayerSquares.push(indexOfSquare);



		$.each(winningCombinations, function(index, combination)
		{
			var hasAllSquare = true;



			$.each(combination, function(index, cell)
			{
				if($.inArray(cell, currentPlayerSquares) === -1)
				{
					$('.cell').text(currentPlayerToken);
					hasAllSquare = false;

				}
			});

			if(hasAllSquare)
			{
				alert(currentPlayerToken + ' wins !');
			}
		});

		if(currentPlayerToken === 'x')
		{
			currentPlayerToken = 'o';
		}
		else
		{
			currentPlayerToken = 'x';
		}
	});
});


